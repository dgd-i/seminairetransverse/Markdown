__Note:__ The document here is the presentation as it was given in the 21th of June 2021. The evolving version of this may be found in the [group dedicated to formations](https://gitlab.inria.fr/formations/markdown/Markdown).

# What is Markdown?

Markdown is a very simple language to format properly plain-text, with an unobstrusive syntax (contrary to html or LaTeX for instance)

It is widely used in tools used by developers

- README of [Gitlab](https://gitlab.inria.fr/solverstack/chameleon) or [GitHub](https://github.com/GUDHI/gudhi-devel) projects
- In Jupyter notebooks (see next talk :wink:)

but is in fact present in many tools used far beyond the circle of developers in Inria:

- [Mattermost](https://mattermost.inria.fr).
- [Wikis](https://gitlab.inria.fr/dgd-i/seminairestransverses/-/wikis/S%C3%A9minaires-transverses).
- The ~~future~~[^pad] [notes.inria.fr](https://notes.inria.fr), which will replace [notepad.inria.fr](https://notepad.inria.fr)

[^pad]: **[MAJ 18/06]:** The service has been [launched officially](https://intranet.inria.fr/Actualite/InfoDSI-Outils-Collaboratifs-NOTES-nouveau-service-de-prise-de-notes-collaboratif) and the shutdown of pad.inria.fr is for the end of September 2021

It can also be used locally in most text editors - files with **md** extension are interpreted as Markdown files.

# Why is it popular?

- As it is simple text, it is very easy to use along a version manager, contrary to a Word document which is a binary.
- Very lightweight documents.
- Very easy learning curve.

# An overview of the commands

## In-built help in tools

Most tools with Markdown offer in-built guides to remind the most essential commands.

- In Mattermost it is by choosing _Help_ > _Formater le texte à l'aide de Markdown_
- In [notes.inria.fr](https://notes.inria.fr) it is by clicking on the help menu when you're editing a note (**not** on the homepage).
- For Gitlab, [this documentation](https://docs.gitlab.com/ee/user/markdown.html)

## Demonstration

In [notes.inria.fr](https://notes.inria.fr), using the content of [this file](./demo.md).

# Caveat

There are actually several flavors of Markdown, so some commands might not work everywhere[^footnote]. You may look at the [Wikipedia page](https://en.wikipedia.org/wiki/Markdown) if you want more about the historic and standardization of Markdown or [this site](https://www.markdownguide.org/extended-syntax/) if you want an in-depth analysis of several different flavors and what each of them entails.


# Conversion to other formats

It is possible to export the generated document in other formats. In [notes.inria.fr](https://notes.inria.fr), it is done directly with the few choices given in the _Menu_.

If you're working locally, you can use [pandoc](https://pandoc.org/) to convert into almost any format you wish.

For instance the command: 

````
pandoc README.md -o Markdown.docx
````

provides an acceptable Word version of the present file.



[^footnote]: For instance this footnote command doesn't work on Mattermost.
 
