# Ordering your document

## Non numbered lists

- First item
- Second item

## Numbered lists

The numbering is rather clever:

1. First numbered item
1. Second numbered item

--- 

# Text formatting

- Italic is obtained by preffixing and suffixing with _ or * (just don't mix them up): either *italic* or _italic_, no *italic_

- **Bold** or __bold__

- ++Underlined++ (doesn't work in all Markdown flavors)

- ~~strikethrough~~



--- 

# Links

It is easy to [insert links](https://gitlab.inria.fr/dgd-i/seminairestransverses/-/wikis/S%C3%A9minaires-transverses) that might be internal (in this case relative paths are used) or external.

Images may be inserted as well with same syntax.




---

# Arrays

|Column 1 | Column 2 | Column 3 |
| :-------- | :--------: | --------: |
| Left-aligned Text     | Centered text     | Right-aligned text  |
| Lorem ipsum dolor | sit amet | consectetur adipiscing elit |

--- 

# When Markdown is not enough...

html can be used.

This is for instance recommended for comments (see this [StackOverflow discussion](https://stackoverflow.com/questions/4823468/comments-in-markdown))

<!---
your comment goes here
and here
-->

or to color some text:

<p style="color:blue">This text should be in blue (if your Markdown flavor support it).</p>

--- 

# Inserting code

```c++
#include <cstdlib>
#include <iostream>

int main(int argc, char** argv)
{
    std::cout << "Hello world" << std::endl;
    exit(EXIT_SUCCESS);
}
```

```python
import sys

print("Hello world")
sys.exit()
```

LaTeX is also supported as code in some Markdown editors (ok for instance in mattermost.inria.fr or  Gitlab wiki, but not in notes.inria.fr):

```latex
\int_{a}^{b} x^2 dx
```

But not in notes.inria.fr, where the following syntax works: $\int_{a}^{b} x^2 dx$









